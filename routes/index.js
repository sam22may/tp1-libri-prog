var express = require('express');
var router = express.Router();
let formations = require('../custom_modules/formations')
let blog = require('../custom_modules/blog')
let contact = require('../custom_modules/contact')

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: "Accueil", description: 'École de formation continue' });
});

router.get('/nosformations', function(req, res, next){
  res.render('formations/nosformations', { title: 'Nos formations', description: 'Nous offrons deux formations:' })
})

router.get('/nosformations/:type', function(req, res, next){
  res.render('formations/formation_par_type', { type: req.params.type, description:'Voici la liste des formations pour:', 
    formation_mongo: formations.getFormationByType(req.params.type),
  })
})

router.get('/nosformations/formation/:id', function(req, res, next){
  res.render('formations/formation', {
    formation:formations.getFormationById(req.params.id)
  })
})

router.get('/contact', function(req, res, next){
  res.render('contact/contact', { title: 'Nous contacter', description: 'Veuillez remplir le formulaire pour nous contacter' })
})

router.post('/contact', function(req, res, next){
  let info = contact.addContact(req.body)
  if(info){
    res.render('contact/contact_post', { title: 'Nous contacter', description: 'Vos informations ont été envoyés, merci!',
    informations: contact.allContact })
  } else {
    res.render('contact/contact_false', { title: 'Nous contacter', description: 'Vous devez remplir le formulaire avec les champs (*) requis',
    informations: contact.allContact,
    msgErreur: contact.allMsgErreur })
  }
})

router.get('/blog', function(req, res, next){
  res.render('blog/blog', { title: 'Blog', description: 'Ceci est une description du blog',
    articles: blog.allArticle })
})

router.get('/blog/blog_article/:id', function(req, res, next){
  res.render('blog/blog_article', { 
    article: blog.getArticleById(req.params.id) })
})


module.exports = router;