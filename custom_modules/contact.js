class Contact{
    constructor(id, nom, prenom, courriel, tel, sujet, champ){
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.courriel = courriel;
        this.tel = tel;
        this.sujet = sujet;
        this.champ = champ;
    }
}

let dataContact = [];

const emailValide = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
const telValide = /^[0-9]{3}-[0-9]{3}-[0-9]{4}$/

let msgErreur = [];

exports.addContact = function(dat){
    dataContact.length = 0
    let id = dataContact.length;
    dataContact.push(new Contact(id, dat.nom, dat.prenom, dat.courriel, dat.tel, dat.sujet, dat.champ))
    console.log(dataContact)
    let email = dataContact[0].courriel
    let tel = dataContact[0].tel
    if(!emailValide.test(email)){
        msgErreur.push("Votre email doit être dans un format valide(email@exemple.com)")
    }
    if(!telValide.test(tel)){
        msgErreur.push("Votre numéro de téléphone doit être dans un format valide(xxx-xxx-xxxx)")
    }
    if(dataContact[0].sujet == ""){
        msgErreur.push("Veuillez sélectionner le sujet de la demande")
    }
    if(dataContact[0].nom == "" || dataContact[0].prenom == "" || dataContact[0].courriel == "" || dataContact[0].tel == "" || dataContact[0].champ == ""){
        msgErreur.push("Les champs avec un astérisque(*) doivent être remplis")
    }
    if(msgErreur.length > 0){
        msgErreur = []
        return false
    }
    console.log(msgErreur)
    return true;
}

exports.allContact = dataContact;
exports.allMsgErreur = msgErreur;