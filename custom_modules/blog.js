class Article{
    constructor(id, titre, date, auteur, contenu){
        this.id = id;
        this.titre = titre;
        this.date = date;
        this.auteur = auteur;
        this.contenu = contenu;
    }
}

const dataArticle = [
    new Article( 0, "Les bases de données du web", "01/03/2010", "Robert Lepage", "Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptas tempore commodi, nemo, ipsam officia nesciunt saepe a illo, asperiores laudantium deserunt molestias. Beatae, autem distinctio quo amet necessitatibus commodi. Labore."),
    new Article( 1, "Entrepôt binaire", "06/06/2006", "Lucifer Morningstar", "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Id cupiditate perspiciatis delectus voluptatibus magni animi obcaecati eaque a tempore. Ad minima nesciunt tempore itaque consequuntur dolor nostrum molestias dolorum voluptatem."),
    new Article( 2, "Comment faire pour stocker nos données", "12/12/2012", "Pere Due", "Je suis perdue dans l'espace, l'article du siècle avec un soupçon de médiocrité prononcé par la plume de mon clavier"),
    new Article( 3, "Tout sur MongoDB", "23/09/2020", "Finn Finault", "Lorem ipsum dolor sit amet consectetur adipisicing elit. Aut ab quisquam voluptatem, voluptate tempora ea doloremque. Modi iusto, vitae doloribus laborum, ipsa, corrupti ex laboriosam eaque enim quo nam impedit!"),
    new Article( 4, "La planète BDD", "30/01/2045", "Mr. Temporelle", "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Laudantium amet perspiciatis perferendis consequuntur ea omnis porro eos, repellendus mollitia dolore cum! Maxime asperiores eligendi quia molestiae, quibusdam hic quae iste?"),
    new Article( 5, "L'article louche", "01/01/1999", "Johny Lambourghini", "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Provident neque dolores exercitationem veniam doloremque, consequatur sit quos quasi eligendi officiis. Quas deleniti quasi est sunt magni perferendis optio repudiandae nesciunt. Lorem, ipsum dolor sit amet consectetur adipisicing elit. Provident neque dolores exercitationem veniam doloremque, consequatur sit quos quasi eligendi officiis. Quas deleniti quasi est sunt magni perferendis optio repudiandae nesciunt."),
    new Article( 6, "Base numérique", "30/09/2002", "Jean Paul Van Dam", "Lorem ipsum dolor sit amet consectetur adipisicing elit. Soluta vel assumenda dolore possimus, consequuntur necessitatibus facere suscipit temporibus a in. Culpa aut accusamus aperiam tempora recusandae impedit aliquid libero voluptas? Lorem ipsum dolor sit amet consectetur adipisicing elit. Soluta vel assumenda dolore possimus, consequuntur necessitatibus facere suscipit temporibus a in. Culpa aut accusamus aperiam tempora recusandae impedit aliquid libero voluptas?"),
    new Article( 7, "Article sur Node.js", "21/08/2019", "Paul Platon", "Lorem ipsum dolor sit amet consectetur adipisicing elit. In provident alias nulla ad optio ducimus quia expedita natus iste consectetur corrupti, culpa error harum animi neque voluptatem nemo atque beatae? Lorem ipsum dolor sit amet consectetur adipisicing elit. In provident alias nulla ad optio ducimus quia expedita natus iste consectetur corrupti, culpa error harum animi neque voluptatem nemo atque beatae?"),
    new Article( 8, "Base de donées pour toujours", "21/12/2021", "Fernand Forever", "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Itaque porro suscipit animi quas voluptatem, beatae laudantium voluptas. Reprehenderit ab magnam quam, nulla quos quidem at aliquid adipisci. Animi, ratione dolorum?"),
    new Article( 9, "L'inspiration n'est pas infinie", "22/02/2021", "Isabelle Poulin", "Lorem ipsum dolor sit amet consectetur adipisicing elit. Tempora, inventore eligendi deleniti, reiciendis blanditiis, eos earum maxime ex accusantium accusamus provident necessitatibus quam officiis voluptatibus nisi. Iure sit est in!")
]

exports.getArticleById = function(id){
    return dataArticle[id]; 
}

exports.allArticle = dataArticle