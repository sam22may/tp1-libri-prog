class Formation{
    constructor(id, nom, desc, duree, type, cout){
        this.id = id;
        this.nom = nom;
        this.desc = desc;
        this.duree = duree;
        this.type = type;
        this.cout = cout;
    }
}

const dataFormation = [   
    new Formation( 0, "Bases de données mongo 1","Lorem ipsum dolor sit, amet consectetur adipisicing elit. Praesentium odit provident minima pariatur voluptatum cum. Sunt, explicabo doloribus iste odit cum beatae, doloremque suscipit sed ratione quo eum labore amet!","180 heures","mongodb","1200$"),
    new Formation( 1, "Introduction à mongodb","Lorem ipsum dolor sit amet consectetur adipisicing elit. A accusamus animi autem sed quibusdam ullam quidem corrupti cupiditate, dolorum dolore non. Nobis at explicabo doloremque ad accusantium aut culpa aliquid.","80 heures","mongodb","600$"),
    new Formation( 2, "Formation Débutant","Lorem ipsum dolor sit amet consectetur adipisicing elit. Cumque optio autem sapiente vel amet quaerat error rerum ex laudantium ea, placeat eligendi officiis ad laboriosam impedit repellat quos fugit praesentium.","100 heures","mongodb","800$"), 
    new Formation( 3, "Formation Intermédiaire","Lorem ipsum dolor sit amet consectetur adipisicing elit. Amet debitis totam itaque pariatur voluptatibus optio cupiditate dolores exercitationem laboriosam deserunt, consequuntur, eum tenetur ducimus accusamus odit! Nulla tempora natus quaerat.","120 heures","mongodb","1000$"),
    new Formation( 4, "Formation Avancé","Lorem ipsum dolor sit amet consectetur adipisicing elit. Vitae voluptates cum nisi. Consequatur explicabo tempora eaque sequi corrupti, laboriosam quas ab voluptatibus deserunt est saepe facilis eligendi earum pariatur voluptate.","200 heures","mongodb","1500$"),
    new Formation( 5, "Tout sur Nodejs","Lorem ipsum dolor sit, amet consectetur adipisicing elit. Praesentium odit provident minima pariatur voluptatum cum. Sunt, explicabo doloribus iste odit cum beatae, doloremque suscipit sed ratione quo eum labore amet!","180 heures","nodejs","1200$"),
    new Formation( 6, "Introduction à Nodejs","Lorem ipsum dolor sit amet consectetur adipisicing elit. A accusamus animi autem sed quibusdam ullam quidem corrupti cupiditate, dolorum dolore non. Nobis at explicabo doloremque ad accusantium aut culpa aliquid.","80 heures","nodejs","600$"),
    new Formation( 7, "Formation Débutant","Lorem ipsum dolor sit amet consectetur adipisicing elit. Cumque optio autem sapiente vel amet quaerat error rerum ex laudantium ea, placeat eligendi officiis ad laboriosam impedit repellat quos fugit praesentium.","100 heures","nodejs","800$"), 
    new Formation( 8, "Formation Intermédiaire","Lorem ipsum dolor sit amet consectetur adipisicing elit. Amet debitis totam itaque pariatur voluptatibus optio cupiditate dolores exercitationem laboriosam deserunt, consequuntur, eum tenetur ducimus accusamus odit! Nulla tempora natus quaerat.","120 heures","nodejs","1000$"),
    new Formation( 9, "Formation Avancé","Lorem ipsum dolor sit amet consectetur adipisicing elit. Vitae voluptates cum nisi. Consequatur explicabo tempora eaque sequi corrupti, laboriosam quas ab voluptatibus deserunt est saepe facilis eligendi earum pariatur voluptate.","200 heures","nodejs","1500$"),
];

exports.getFormationById = function(id){
   return dataFormation[id]; 
}

// let dataFormation_nodejs = []
// let dataFormation_mongo = []
// function getFormationByType(){
//     for(let i = 0; i < dataFormation.length; i++){
//         if(dataFormation[i].type == "mongodb"){
//             dataFormation_mongo.push(new Formation(dataFormation[i].id, dataFormation[i].nom, dataFormation[i].desc, dataFormation[i].duree, dataFormation[i].type, dataFormation[i].cout))
//         } else if(dataFormation[i].type == "nodejs"){
//             dataFormation_nodejs.push(new Formation(dataFormation[i].id, dataFormation[i].nom, dataFormation[i].desc, dataFormation[i].duree, dataFormation[i].type, dataFormation[i].cout))
//         }
//     }
// }
// getFormationByType()
// exports.allFormation_mongo = dataFormation_mongo
// exports.allFormation_nodejs = dataFormation_nodejs



// abstraction 
exports.getFormationByType = function(type){
    let dataType = []
    for(let i = 0; i < dataFormation.length; i++){
        if(dataFormation[i].type == type){
            dataType.push(dataFormation[i])
        }
    }
    return dataType
}